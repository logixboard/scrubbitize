/* This file is part of scrubbitize
 *
 * Copyright 2021 Logixboard Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without
 * fee is hereby granted, provided that the above copyright notice and this permission notice
 * appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

import faker from 'faker'
import { QueryConfig } from 'pg'

import { Column, ColumnComment, TableComment } from './sql-util'

export type SqlRow = Record<any, unknown>

export enum ScrubberScope {
    TABLE = 'TABLE',
    COLUMN = 'COLUMN',
}

export type TableScrubberMethod = (
    args: boolean | string[],
    ctx: TableComment,
    batch: SqlRow[],
    tablePks: Column[],
) => QueryConfig

export interface TableScrubber {
    scope: ScrubberScope.TABLE
    scrubQuery: TableScrubberMethod
}

export type ColumnScrubberMethod = (
    args: boolean | string[],
    cxt: ColumnComment,
    batch: SqlRow[],
    tablePks: Column[],
) => QueryConfig

export interface ColumnScrubber {
    scope: ScrubberScope.COLUMN
    scrubQuery: ColumnScrubberMethod
}

export type Scrubber = ColumnScrubber | TableScrubber

export function newTableScrubber(scrubber: TableScrubberMethod): TableScrubber {
    return {
        scope: ScrubberScope.TABLE,
        scrubQuery: scrubber,
    }
}

export function newColumnScrubber(
    scrubber: ColumnScrubberMethod,
): ColumnScrubber {
    return {
        scope: ScrubberScope.COLUMN,
        scrubQuery: scrubber,
    }
}

// can't use sureql for these query generators since the query _structure_ has
// to be generated at runtime (somewhat like having to run FORMAT in plpgsql)

const nullifyUpdate = (
    schema: string,
    table: string,
    pk: Column,
    from: Column,
) => `
UPDATE "${schema}"."${table}"
SET ${from.name} = NULL
FROM (
    SELECT unnest($1::${pk.coltype}[]) AS ${pk.name}
) AS requested
WHERE "${schema}"."${table}"."${pk.name}" = requested.${pk.name}
`

// forcibly casting the pk type is required because, for example, we'll have a
// copy of a UUID as a string, but PG's type coercion only goes one layer deep.
// while `SELECT * FROM mytable WHERE id = 'uuidhere'` works,
// `ARRAY['uuidhere']` has type `text[]`, and thus can't be used to update
// against UUID columns
const oneToOneUnnestingUpdate = (
    schema: string,
    table: string,
    pk: Column,
    from: Column,
) => `
UPDATE "${schema}"."${table}"
SET ${from.name} = requested.${from.name}
FROM (
    SELECT unnest($1::${pk.coltype}[]) AS ${pk.name},
           unnest($2::${from.coltype}[]) AS ${from.name}
) AS requested
WHERE "${schema}"."${table}"."${pk.name}" = requested.${pk.name}
`

function oneToOneUnnestingUpdateColumnScrub(
    ctx: ColumnComment,
    batch: SqlRow[],
    tablePks: Column[],
    values: [unknown[], unknown[]],
): QueryConfig {
    if (values[0].length !== values[1].length) {
        throw new Error(
            '1:1 unnesting updates require symmetrical value arrays',
        )
    }

    const uniqueField = findPreferredUniqueField(tablePks, batch)

    return {
        name: columnScrubberQueryConfigName(ctx),
        text: oneToOneUnnestingUpdate(
            ctx.tableschema,
            ctx.tablename,
            uniqueField,
            ctx.column,
        ),
        values,
    }
}

function findPreferredUniqueField(tablePks: Column[], batch: SqlRow[]): Column {
    const preferred = tablePks.find(pk => pk.name === 'id') || tablePks[0]
    if (batch.find(row => !Object.keys(row).includes(preferred.name))) {
        throw new Error('batch includes row lacking the detected uniqueField')
    }
    return preferred
}

function preferredUniqueFieldsInBatch(
    batch: SqlRow[],
    tablePks: Column[],
): unknown[] {
    const uniqueField = findPreferredUniqueField(tablePks, batch)
    return batch.map(row => row[uniqueField.name])
}

function columnScrubberQueryConfigName(ctx: ColumnComment): string {
    return `scrubbitize_scrub_col_${ctx.tableschema}_${ctx.tablename}_${ctx.column.name}`
}

export const columnScrubbers = {
    nullify: newColumnScrubber((_, ctx, batch, tablePks) => {
        const uniqueField = findPreferredUniqueField(tablePks, batch)
        return {
            name: columnScrubberQueryConfigName(ctx),
            text: nullifyUpdate(
                ctx.tableschema,
                ctx.tablename,
                uniqueField,
                ctx.column,
            ),
            values: [preferredUniqueFieldsInBatch(batch, tablePks)],
        }
    }),
    fixedString: newColumnScrubber((args, ctx, batch, tablePks) =>
        oneToOneUnnestingUpdateColumnScrub(ctx, batch, tablePks, [
            preferredUniqueFieldsInBatch(batch, tablePks),
            new Array(batch.length).fill(
                Array.isArray(args) ? args[0] || '' : '',
            ),
        ]),
    ),
    email: newColumnScrubber((_, ctx, batch, tablePks) =>
        oneToOneUnnestingUpdateColumnScrub(ctx, batch, tablePks, [
            preferredUniqueFieldsInBatch(batch, tablePks),
            // explicitly using exampleEmail to generate example.<tld> emails,
            // which are (usually) reserved domain per the RFCs. the other
            // email method generates *too* realistic of emails, eg.
            // hotmail/yahoo/gmail/etc. domains
            batch.map(() => faker.internet.exampleEmail()),
        ]),
    ),
    name: newColumnScrubber((_, ctx, batch, tablePks) =>
        oneToOneUnnestingUpdateColumnScrub(ctx, batch, tablePks, [
            preferredUniqueFieldsInBatch(batch, tablePks),
            batch.map(() => faker.name.findName()),
        ]),
    ),
    phone: newColumnScrubber((_, ctx, batch, tablePks) =>
        oneToOneUnnestingUpdateColumnScrub(ctx, batch, tablePks, [
            preferredUniqueFieldsInBatch(batch, tablePks),
            batch.map(() => faker.phone.phoneNumber()),
        ]),
    ),
}
