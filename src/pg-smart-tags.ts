// lifted (and enhanced) from
// https://github.com/graphile/graphile-engine/blob/9d6c29e3505844ca64020fb6850093a7678a0fa4/packages/graphile-build-pg/src/utils.ts
// and thus:

/*
The MIT License (MIT)

Copyright © `2018` Benjie Gillam
Copyright 2021 Logixboard, Inc.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

type TagData = string | string[] | boolean

interface Tags {
    tags: Record<string, TagData>
    text: string
}

export function parseTags(str: string): Tags {
    return str.split(/\r?\n/).reduce(
        (prev, curr) => {
            if (prev.text !== '') {
                return { ...prev, text: `${prev.text}\n${curr}` }
            }
            const match = curr.match(
                /^@[a-zA-Z][a-zA-Z0-9_]*(\/[a-zA-Z0-9_]+)?($|\s)/,
            )
            if (!match) {
                return { ...prev, text: curr }
            }
            const key = match[0].substr(1).trim()
            const value = match[0] === curr ? true : curr.replace(match[0], '')
            return {
                ...prev,
                tags: {
                    ...prev.tags,
                    [key]: mergeTag(prev, key, value),
                },
            }
        },
        {
            tags: {},
            text: '',
        },
    )
}

function mergeTag(prev: Tags, key: string, value: boolean | string): TagData {
    if (!Object.prototype.hasOwnProperty.call(prev.tags, key)) {
        return value
    }

    if (typeof value !== 'boolean') {
        const oldData = prev.tags[key]
        if (Array.isArray(oldData)) {
            return [...oldData, value]
        } else if (typeof oldData !== 'boolean') {
            return [oldData, value]
        }
    }

    throw new Error(
        `exhausted all merge options, cannot handle type ${typeof value}`,
    )
}
