/* This file is part of scrubbitize
 *
 * Copyright 2021 Logixboard Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without
 * fee is hereby granted, provided that the above copyright notice and this permission notice
 * appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

import { Pool, PoolClient, PoolConfig, QueryConfig, QueryResult } from 'pg'
import { parse as parsePgConnectionString } from 'pg-connection-string'
import { v4 as uuidv4 } from 'uuid'

import { parseTags } from './pg-smart-tags'
import {
    selectColumnComments,
    selectTableComments,
    selectTablePk,
} from './sql/generated'

const SMART_TAG_PREFIX = 'scrubbitize/'
const SMART_TAG_PREFIX_REGEXP = /^scrubbitize\//

export interface TableComment {
    tableschema: string
    tablename: string
    description: string
    parsedTags: ReturnType<typeof parseTags>
}

export type ColumnComment = TableComment & {
    column: Column
}

export interface Column {
    name: string
    coltype: string
}

interface Environment {
    PGHOST?: string
    PGHOSTADDR?: string
    PGPORT?: string
    PGDATABASE?: string
    PGUSER?: string
    PGPASSWORD?: string
    DATABASE_URL?: string
    DB_CONNECTION_STRING?: string
    DB_MAX_CONNS?: string
    DB_IDLE_TIMEOUT_MILLIS?: string
}

// lifted/adapted from https://github.com/graphile/postgraphile/blob/dd2b6b2b364efd015e46cd1faab4d37d2fd9fbad/src/postgraphile/cli.ts#L534
export function getPgConfigFromEnvironment(env: Environment): PoolConfig {
    const dbUrl = env.DATABASE_URL || env.DB_CONNECTION_STRING || ''
    return {
        // If we have a Postgres connection string, parse it and use that as our
        // config. If we don’t have a connection string use some environment
        // variables or final defaults. Other environment variables should be
        // detected and used by `pg`.
        ...(dbUrl
            ? coercePoolConfig(parsePgConnectionString(dbUrl))
            : {
                  host: env.PGHOST || env.PGHOSTADDR || 'localhost',
                  port: parseIntWithDefault(env.PGPORT, 5432),
                  database: env.PGDATABASE,
                  user: env.PGUSER,
                  password: env.PGPASSWORD,
              }),
        // Add the max pool size to our config.
        max: parseIntWithDefault(env.DB_MAX_CONNS, 15),
        idleTimeoutMillis: parseIntWithDefault(env.DB_IDLE_TIMEOUT_MILLIS, 500),
    }
}

// Work around type mismatches between parsePgConnectionString and PoolConfig
function coercePoolConfig(
    opts: ReturnType<typeof parsePgConnectionString>,
): PoolConfig {
    /* eslint-disable camelcase */
    return {
        ...opts,
        application_name: opts['application_name'] || undefined,
        // eslint-disable-next-line eqeqeq,no-negated-condition
        ssl: opts.ssl != null ? !!opts.ssl : undefined,
        user: typeof opts.user === 'string' ? opts.user : undefined,
        database: typeof opts.database === 'string' ? opts.database : undefined,
        password: typeof opts.password === 'string' ? opts.password : undefined,
        port:
            // eslint-disable-next-line no-nested-ternary
            typeof opts.port === 'number'
                ? opts.port
                : typeof opts.port === 'string'
                ? parseInt(opts.port, 10)
                : undefined,
        host: typeof opts.host === 'string' ? opts.host : undefined,
    }
    /* eslint-enable camelcase */
}

function parseIntWithDefault(
    input: string | undefined,
    defaultValue: number,
): number {
    return (input && parseInt(input, 10)) || defaultValue
}

function enforceScrubberTagLimit(
    comments: Pick<TableComment, 'parsedTags'>[],
): void {
    for (const comment of comments) {
        if (
            Object.keys(comment.parsedTags.tags).filter(tag =>
                tag.startsWith(SMART_TAG_PREFIX),
            ).length > 1
        ) {
            throw new Error(
                'only one scrubbitizer tag is currently supported per entity',
            )
        }
    }
}

export function firstScrubInstruction(
    tags: TableComment['parsedTags'],
): [string, any] {
    const tag = Object.keys(tags.tags).find(tag =>
        tag.startsWith(SMART_TAG_PREFIX),
    )

    if (!tag) {
        throw new Error('tags do not include scrubbitizer instructions')
    }

    return [tag.replace(SMART_TAG_PREFIX_REGEXP, ''), tags.tags[tag]]
}

// while our query will return _all_ comments throughout the entire DB, we only
// care about those which have Smart Tags that are under our prefix (and not,
// perhaps, Postgraphile tags). Array.find returns either the first match or
// undefined, making `filter` happy, and trimming our overall list down
export async function relevantTableComments(
    conn: Pool | PoolClient,
): Promise<TableComment[]> {
    const ret: TableComment[] = (await conn.query(selectTableComments({}))).rows
        .map(tbl => ({
            ...tbl,
            parsedTags: parseTags(tbl.description),
        }))
        .filter(({ parsedTags }) =>
            Object.keys(parsedTags.tags).find(tag =>
                tag.startsWith(SMART_TAG_PREFIX),
            ),
        )

    enforceScrubberTagLimit(ret)

    return ret
}

// while our query will return _all_ comments throughout the entire DB, we only
// care about those which have Smart Tags that are under our prefix (and not,
// perhaps, Postgraphile tags). Array.find returns either the first match or
// undefined, making `filter` happy, and trimming our overall list down
export async function relevantColumnComments(
    conn: Pool | PoolClient,
): Promise<ColumnComment[]> {
    const ret: ColumnComment[] = (
        await conn.query(selectColumnComments({}))
    ).rows
        .map(col => ({
            ...col,
            column: { name: col.columnname, coltype: col.columntype },
            parsedTags: parseTags(col.description),
        }))
        .filter(({ parsedTags }) =>
            Object.keys(parsedTags.tags).find(tag =>
                tag.startsWith(SMART_TAG_PREFIX),
            ),
        )

    enforceScrubberTagLimit(ret)

    return ret
}

export async function pksOfTable(
    conn: Pool | PoolClient,
    schema: string,
    table: string,
): Promise<Column[]> {
    return (await conn.query(selectTablePk({ schema, tableName: table }))).rows
}

// https://github.com/brianc/node-postgres/issues/2140#issuecomment-616638067,
// mostly because pg-cursor upstream doesn't ship typedefs, and also seems to
// have some odd behavior at times, so why not give this a whirl
export async function* chunkedQuery(
    client: Pool | PoolClient,
    query: QueryConfig,
    chunkSize: number,
): AsyncGenerator<QueryResult> {
    const name = uuidv4() // or some unique name

    try {
        await client.query('BEGIN')
        await client.query(
            `DECLARE "${name}" NO SCROLL CURSOR FOR ${query.text}`,
            query.values,
        )

        while (true) {
            const record = await client.query(`FETCH ${chunkSize} "${name}"`)

            if (record.rows.length === 0) {
                break
            }

            yield record
        }
        await client.query('COMMIT')
    } catch (err) {
        await client.query('ROLLBACK')
        throw err
    }
}
