/* This file is part of scrubbitize
 *
 * Copyright 2021 Logixboard Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without
 * fee is hereby granted, provided that the above copyright notice and this permission notice
 * appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

import { rmdir as _rmdir, unlink as _unlink } from 'fs'
import os from 'os'
import path from 'path'
import { promisify } from 'util'

import execa from 'execa'
import {
    number as argNumber,
    string as argString,
    command,
    run,
    option,
} from 'cmd-ts'
import { Pool, QueryConfig } from 'pg'
import tmp from 'tmp-promise'

import { columnScrubbers } from './scrubbers'
import {
    chunkedQuery,
    firstScrubInstruction,
    getPgConfigFromEnvironment,
    relevantColumnComments,
    relevantTableComments,
    pksOfTable,
} from './sql-util'

const rmdir = promisify(_rmdir)
const unlink = promisify(_unlink)

const RUNTIME_UNIX_TS = Date.now()

const PG_ENABLE_ROW_SECURITY = '--enable-row-security'
const PG_JOBS = (count = 1) => `-j${count}`
const PG_FILE_NOFLAG = (tmpdir: string) =>
    path.join(tmpdir, `pg_dumpall_out_${RUNTIME_UNIX_TS}`)
const PG_FILE = (tmpdir: string) => ['-f', PG_FILE_NOFLAG(tmpdir)]

// https://www.thatguyfromdelhi.com/2017/03/using-pgdumpall-with-aws-rds-postgres.html
const PGDUMPALL_RDS_FRIENDLY = ['--globals-only', '--no-role-passwords']

const PGDUMP_FORMAT_DIRECTORY = '-Fd'
const PGDUMP_COMPRESSION_LEVEL = (level: number) => `-Z${level}`
// necessary because the working DB will only have a stub copy of the roles and
// users that existed in the source DB, and that results in hidden tables if
// ownership is preserved
const PGDUMP_EXCLUDE_TABLE_OWNERSHIP = '--no-owner'
const PGDUMP_INCLUDE_BLOBS = '--blobs'
const PGDUMP_FILE_NOFLAG = (tmpdir: string) =>
    path.join(tmpdir, `pg_dump_out_${RUNTIME_UNIX_TS}`)
const PGDUMP_FILE = (tmpdir: string) => ['-f', PGDUMP_FILE_NOFLAG(tmpdir)]

const PSQL_STOP_ON_ERROR = ['-v', 'ON_ERROR_STOP=1']

interface Arguments {
    // we need this as a bare string and don't want to immediately convert to a
    // PoolConfig (which cmd-ts would support!) so that we can pass it along
    // verbatim to `pg_dump` and not have to reimplement its featureset
    sourceConnectionString: string
    workingConnectionString: string

    dryRun: boolean
    batchSize: number
}

const cmd = command({
    name: 'scrubbitize',
    description: 'produce a sanitized dump of a SQL database',
    version: '0.1.0',
    args: {
        batchSize: option({
            type: argNumber,
            description:
                'batch (cursor) size when processing rows in working copy',
            long: 'batch-size',
            short: 'b',
            defaultValue: () => 100,
            defaultValueIsSerializable: true,
        }),
        sourceConnectionString: option({
            type: argString,
            description:
                'source DB connection string in libpq connstring format (eg. postgresql://[userspec@][hostspec][/dbname][?paramspec]). pulled from DB_CONNECTION_STRING or DATABASE_URL in environment by default',
            long: 'source-connection',
            short: 's',
        }),
        workingConnectionString: option({
            type: argString,
            description:
                'working-copy DB connection string in libpq connstring format (eg. postgresql://[userspec@][hostspec][/dbname][?paramspec]). pulled from DB_CONNECTION_STRING or DATABASE_URL in environment by default',
            long: 'working-copy-connection',
            short: 'w',
        }),
    },
    handler: main,
})

run(cmd, process.argv.slice(2))

async function main(args: Arguments): Promise<void> {
    await transferDBs(args.sourceConnectionString, args.workingConnectionString)

    const conn = new Pool(
        getPgConfigFromEnvironment({
            DB_CONNECTION_STRING: args.workingConnectionString,
        }),
    )

    const tableComments = await relevantTableComments(conn)
    const columnComments = await relevantColumnComments(conn)

    for (const column of columnComments) {
        const [instruction, instructionArgs] = firstScrubInstruction(
            column.parsedTags,
        )

        // this looks like a somewhat comical way to solve this problem, but it
        // allows a type-safe lookup of the dictionary (which doesn't have an
        // index type since all members are known at compile time)
        const scrubberEntry = Object.entries(columnScrubbers).find(
            ([name]) => name === instruction,
        )
        if (!scrubberEntry) {
            throw new Error(
                `unsupported column scrub instruction ${instruction} requested`,
            )
        }
        const [, scrubber] = scrubberEntry

        const schema = column.tableschema
        const table = column.tablename

        const tablePks = await pksOfTable(conn, schema, table)

        const query: QueryConfig = {
            name: 'get_all_rows',
            text: `SELECT * FROM "${schema}"."${table}"`,
        }

        for await (const batch of chunkedQuery(conn, query, args.batchSize)) {
            const scrub = scrubber.scrubQuery(
                instructionArgs,
                column,
                batch.rows,
                tablePks,
            )
            await conn.query(scrub)
        }
    }
}

async function transferDBs(sourceDB: string, targetDB: string): Promise<void> {
    await tmp.withDir(async tmpdir => {
        await backupRoles(tmpdir.path, sourceDB)
        console.log('backup complete: roles')
        await backupData(tmpdir.path, sourceDB)
        console.log('backup complete: data')
        await restoreRoles(tmpdir.path, targetDB)
        console.log('restore complete: roles')
        await restoreData(tmpdir.path, targetDB)
        console.log('restore complete: data')

        await Promise.all([
            unlink(PG_FILE_NOFLAG(tmpdir.path)),
            rmdir(PGDUMP_FILE_NOFLAG(tmpdir.path), { recursive: true }),
        ])
        console.log('cleanup complete: tmpdir removal')
    })
}

async function backupRoles(tmppath: string, connstr: string): Promise<string> {
    const dumpRolesExtendedArgs: string[] = [
        ...PGDUMPALL_RDS_FRIENDLY,
        ...PG_FILE(tmppath),
    ]

    try {
        await execa('pg_dumpall', ['-d', connstr, ...dumpRolesExtendedArgs])

        return PG_FILE_NOFLAG(tmppath)
    } catch (err) {
        console.error('failed to backup roles and globals', err)
        throw err
    }
}

async function backupData(tmppath: string, connstr: string): Promise<string> {
    const dumpExtendedArgs: string[] = [
        PGDUMP_COMPRESSION_LEVEL(9),
        PGDUMP_INCLUDE_BLOBS,
        PGDUMP_EXCLUDE_TABLE_OWNERSHIP,
        PGDUMP_FORMAT_DIRECTORY,
        PG_JOBS(os.cpus().length),

        PG_ENABLE_ROW_SECURITY,

        ...PGDUMP_FILE(tmppath),
    ]

    try {
        await execa('pg_dump', [connstr, ...dumpExtendedArgs])
        return PGDUMP_FILE_NOFLAG(tmppath)
    } catch (err) {
        console.error('failed to backup data', err)
        throw err
    }
}

async function restoreRoles(tmppath: string, connstr: string): Promise<void> {
    const restoreRolesExtendedArgs: string[] = [
        ...PG_FILE(tmppath),
        ...PSQL_STOP_ON_ERROR,
    ]

    try {
        await execa('psql', [connstr, ...restoreRolesExtendedArgs])
    } catch (err) {
        console.error('failed to restore roles', err)
        throw err
    }
}

async function restoreData(tmppath: string, connstr: string): Promise<void> {
    const restoreExtendedArgs: string[] = [
        PG_ENABLE_ROW_SECURITY,
        PG_JOBS(os.cpus().length),

        PGDUMP_FILE_NOFLAG(tmppath),
    ]

    try {
        await execa('pg_restore', ['-d', connstr, ...restoreExtendedArgs])
    } catch (err) {
        console.error('failed to restore data', err)
        throw err
    }
}
