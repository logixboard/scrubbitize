import { QueryConfig as PgQuery } from 'pg'

import { MissingValueError } from './common'

export const argumentPattern =
    /(?<prefix>::?)(?<quote>['"]?)(?<key>[a-zA-Z0-9_]+)\k<quote>/g
export const rawQuery = `-- selectColumnComments
-- thanks https://stackoverflow.com/a/4946306
SELECT c.table_schema as tableschema
     , c.table_name as tablename
     , c.column_name as columnname
     , c.data_type as columntype
     , pgd.description
 FROM  pg_catalog.pg_statio_all_tables as st
   inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)
   inner join information_schema.columns c on (pgd.objsubid=c.ordinal_position
     and  c.table_schema=st.schemaname and c.table_name=st.relname);
`

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InputParameters {}

export default function generateQuery(
    parameters: Readonly<InputParameters>,
): PgQuery {
    const values: any[] = []
    const text = rawQuery.replace(
        argumentPattern,
        (_, prefix: string, _quote: string, key: string) => {
            if (prefix === '::') {
                return prefix + key
            } else if (key in parameters) {
                // for each named value in the query, replace with a
                // positional placeholder and accumulate the value in
                // the values list
                values.push(parameters[key as keyof InputParameters])
                return `$${values.length}`
            }

            throw new MissingValueError(key, rawQuery)
        },
    )
    return {
        text,
        values,
        name: 'selectColumnComments',
    }
}
