import selectColumnComments from './selectColumnComments'
import selectTableComments from './selectTableComments'
import selectTablePk from './selectTablePk'

export { selectColumnComments, selectTableComments, selectTablePk }
