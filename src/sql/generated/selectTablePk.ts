import { QueryConfig as PgQuery } from 'pg'

import { MissingValueError } from './common'

export const argumentPattern =
    /(?<prefix>::?)(?<quote>['"]?)(?<key>[a-zA-Z0-9_]+)\k<quote>/g
export const rawQuery = `-- selectTablePk
-- thanks https://stackoverflow.com/a/20537829
SELECT
  pg_attribute.attname as name,
  format_type(pg_attribute.atttypid, pg_attribute.atttypmod) as coltype
FROM pg_index, pg_class, pg_attribute, pg_namespace
WHERE
  pg_class.oid = :'tableName'::regclass AND
  indrelid = pg_class.oid AND
  nspname = :'schema' AND
  pg_class.relnamespace = pg_namespace.oid AND
  pg_attribute.attrelid = pg_class.oid AND
  pg_attribute.attnum = any(pg_index.indkey)
 AND indisprimary;
`

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InputParameters {
    tableName: any
    schema: any
}

export default function generateQuery(
    parameters: Readonly<InputParameters>,
): PgQuery {
    const values: any[] = []
    const text = rawQuery.replace(
        argumentPattern,
        (_, prefix: string, _quote: string, key: string) => {
            if (prefix === '::') {
                return prefix + key
            } else if (key in parameters) {
                // for each named value in the query, replace with a
                // positional placeholder and accumulate the value in
                // the values list
                values.push(parameters[key as keyof InputParameters])
                return `$${values.length}`
            }

            throw new MissingValueError(key, rawQuery)
        },
    )
    return {
        text,
        values,
        name: 'selectTablePk',
    }
}
