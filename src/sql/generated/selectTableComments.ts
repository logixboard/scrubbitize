import { QueryConfig as PgQuery } from 'pg'

import { MissingValueError } from './common'

export const argumentPattern =
    /(?<prefix>::?)(?<quote>['"]?)(?<key>[a-zA-Z0-9_]+)\k<quote>/g
export const rawQuery = `-- selectTableComments
SELECT t.table_schema as tableschema
     , t.table_name as tablename
     , pg_catalog.obj_description(pgc.oid, 'pg_class') as description
  FROM information_schema.tables t
INNER JOIN pg_catalog.pg_class pgc
        ON t.table_name = pgc.relname
WHERE t.table_type='BASE TABLE'
  AND pg_catalog.obj_description(pgc.oid, 'pg_class') IS NOT NULL;
`

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InputParameters {}

export default function generateQuery(
    parameters: Readonly<InputParameters>,
): PgQuery {
    const values: any[] = []
    const text = rawQuery.replace(
        argumentPattern,
        (_, prefix: string, _quote: string, key: string) => {
            if (prefix === '::') {
                return prefix + key
            } else if (key in parameters) {
                // for each named value in the query, replace with a
                // positional placeholder and accumulate the value in
                // the values list
                values.push(parameters[key as keyof InputParameters])
                return `$${values.length}`
            }

            throw new MissingValueError(key, rawQuery)
        },
    )
    return {
        text,
        values,
        name: 'selectTableComments',
    }
}
