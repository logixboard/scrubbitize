-- selectTablePk
-- thanks https://stackoverflow.com/a/20537829
SELECT
  pg_attribute.attname as name,
  format_type(pg_attribute.atttypid, pg_attribute.atttypmod) as coltype
FROM pg_index, pg_class, pg_attribute, pg_namespace
WHERE
  pg_class.oid = :'tableName'::regclass AND
  indrelid = pg_class.oid AND
  nspname = :'schema' AND
  pg_class.relnamespace = pg_namespace.oid AND
  pg_attribute.attrelid = pg_class.oid AND
  pg_attribute.attnum = any(pg_index.indkey)
 AND indisprimary;
