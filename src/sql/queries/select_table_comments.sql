-- selectTableComments
SELECT t.table_schema as tableschema
     , t.table_name as tablename
     , pg_catalog.obj_description(pgc.oid, 'pg_class') as description
  FROM information_schema.tables t
INNER JOIN pg_catalog.pg_class pgc
        ON t.table_name = pgc.relname
WHERE t.table_type='BASE TABLE'
  AND pg_catalog.obj_description(pgc.oid, 'pg_class') IS NOT NULL;
