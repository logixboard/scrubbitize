-- selectColumnComments
-- thanks https://stackoverflow.com/a/4946306
SELECT c.table_schema as tableschema
     , c.table_name as tablename
     , c.column_name as columnname
     , c.data_type as columntype
     , pgd.description
 FROM  pg_catalog.pg_statio_all_tables as st
   inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)
   inner join information_schema.columns c on (pgd.objsubid=c.ordinal_position
     and  c.table_schema=st.schemaname and c.table_name=st.relname);
