module.exports = {
    extends: ["@logixboard/eslint-config-typescript"],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        project: "./tsconfig.base.json",
        tsconfigRootDir: __dirname,
    },
    rules: {
        "no-extra-semi": "error",
    },
}
