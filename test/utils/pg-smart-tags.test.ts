// lifted (and mocha-/chai-ified) from
// https://github.com/graphile/graphile-engine/blob/9d6c29e3505844ca64020fb6850093a7678a0fa4/packages/graphile-build-pg/__tests__/tags.test.js
// and thus:

/*
The MIT License (MIT)

Copyright © `2018` Benjie Gillam
Copyright 2021 Logixboard, Inc.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import { expect } from 'chai'
import 'mocha'

import { parseTags } from '../../src/pg-smart-tags'

describe('parseTags()', () => {
    it('tags are removed correctly', () => {
        expect(
            parseTags(
                `@deprecated Persons now have first and last name\nThe person's name`,
            ).text,
        ).to.equal(`The person's name`)
        expect(
            parseTags(`@deprecated Persons now have first and last name`).text,
        ).to.equal(``)
        expect(parseTags(`@foo\n@bar\nBaz`).text).to.equal(`Baz`)
        expect(parseTags(` @foo @bar\nBaz`).text).to.equal(` @foo @bar\nBaz`)
        expect(parseTags(`@foo\n @bar\nBaz`).text).to.equal(` @bar\nBaz`)
        expect(parseTags(`@foo@bar Baz`).text).to.equal(`@foo@bar Baz`)
        expect(parseTags(`Blah blah @deprecated`).text).to.equal(
            `Blah blah @deprecated`,
        )
        expect(parseTags(`Blah blah\n@deprecated`).text).to.equal(
            `Blah blah\n@deprecated`,
        )
        expect(
            parseTags(
                `@jsonField date timestamp\n@jsonField name text\n@jsonField episode enum ONE=1 TWO=2\nBaz`,
            ).text,
        ).to.equal(`Baz`)
    })

    it('tags are extracted correctly', () => {
        expect(
            parseTags(
                `@deprecated Persons now have first and last name\nThe person's name`,
            ).tags,
        ).to.deep.equal({ deprecated: 'Persons now have first and last name' })
        expect(
            parseTags(`@deprecated Persons now have first and last name`).tags,
        ).to.deep.equal({ deprecated: 'Persons now have first and last name' })
        expect(parseTags(`@foo\n@bar\nBaz`).tags).to.deep.equal({
            foo: true,
            bar: true,
        })
        expect(
            parseTags(`@foo\n@bar\n@scrubbitize/scrubWith email\nBaz`).tags,
        ).to.deep.equal({
            foo: true,
            bar: true,
            'scrubbitize/scrubWith': 'email',
        })
        expect(parseTags(` @foo @bar\nBaz`).tags).to.deep.equal({})
        expect(parseTags(`@foo\n @bar\nBaz`).tags).to.deep.equal({ foo: true })
        expect(parseTags(`@foo@bar Baz`).tags).to.deep.equal({})
        expect(parseTags(`Blah blah @deprecated`).tags).to.deep.equal({})
        expect(parseTags(`Blah blah\n@deprecated`).tags).to.deep.equal({})
        expect(
            parseTags(
                `@jsonField date timestamp\n@jsonField name text\n@jsonField episode enum ONE=1 TWO=2\nBaz`,
            ).tags,
        ).to.deep.equal({
            jsonField: [
                'date timestamp',
                'name text',
                'episode enum ONE=1 TWO=2',
            ],
        })
        expect(
            parseTags(
                `@jsonField date timestamp\r\n@jsonField name text\n@jsonField episode enum ONE=1 TWO=2\r\nBaz`,
            ).tags,
        ).to.deep.equal({
            jsonField: [
                'date timestamp',
                'name text',
                'episode enum ONE=1 TWO=2',
            ],
        })
    })
})
