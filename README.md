# scrubbitize: sanitizing sensitive postgresql backups in a relationally-safe way

> While this directory is currently embedded within `farmers-market` and does take one (replaceable
> and/or inlineable) dependency on a function in `common`, it should generally avoid using
> proprietary Logixboard code - it's a rather logical fit for eventual public open-sourcing.

`scrubbitize` is a tool to sanitize PII or other sensitive data from a PostgreSQL backup, as
dictated [Smart Comments, as borrowed from
Postgraphile](https://www.graphile.org/postgraphile/smart-comments/) and an underlying JS
implementation of each "scrubber". Its goal is to generate database restoration scripts that are
reasonably safe to store in S3 or LFS and to have on developer workstations, and can stand up a
local database with closer-to-real data wherever possible.

## Prerequisites

- Of course, NodeJS 12+
- A source database, presumably an RDS instance, running at least PostgreSQL 12
- A "working copy" database, which can be any similarly-versioned PostgreSQL instance. Another RDS
  instance or a local or containerized DB is fine. This database should be more or less empty, and
  capable of storing the entire source dataset
- Matching versions of `psql`, `pg_dump`, and `pg_restore` available in `$PATH` (this is almost
  always the case if Postgres is installed system-wide)
- Smart Comments on any tables or columns thereof needing modification before the final backup is
  created (sure, this is technically optional, but you're not just looking for a pretty wrapper
  around `pg_dump` and `pg_restore`, are you?)

## Scrubbers Provided

All of these scrubbers are requested using the `@scrubbitize/<scrubber> args args args` smart
commment format.  For example:

```sql
COMMENT ON COLUMN public.user.email IS E'@scrubbitize/email'
```

These can safely live alongside existing Postgraphile Smart Comments, for example:

```sql
COMMENT ON COLUMN public.user.email IS E'@omit update\n@scrubbitize/email'
```

### Table Scope

```sql
COMMENT ON TABLE someschema.sometable IS E'@scrubbitize/nameofscrubber'
```

Table-scoped scrubbers are primarily useful for excluding data. An example of
what they could be used for can currently be found [in a GitLab MR
comment](https://gitlab.com/logixboard/scrubbitize/-/merge_requests/1#note_677793958).

### Column Scope

```sql
COMMENT ON COLUMN someschema.sometable.somecolumnn IS E'@scrubbitize/nameofscrubber'
```

#### `email`

Update the column to a random email address. There is no way to influence the randomness (and thus
result), short of tinkering with NodeJS's entropy system. For a non-random alternative, see
`fixedString`.

#### `fixedString`

Update the column to a hard-coded string. This is mostly useful for non-unique fields that have no
useful meaning after sensitive data has been removed. For example, for a table tracking cities in
the Pacific Northwest (let's imagine this data is sensitive), a `typical_weather` column would be a
decent candidate for `fixedString` redaction: `@scrubbitize/fixedString rainy`.

#### `name`

Update the column to a random, western-style first and last name (blatantly disregarding the
warnings of [Falsehoods Programmers Believe About
Names](https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/), I
know...). There is no way to influence the randomness (and thus result), short of tinkering with
NodeJS's entropy system. For a non-random alternative, see `fixedString`.

#### `nullify`

Update all values in this column to `NULL`. This is useful for, for example,
wholesale clearing out third-party service identifiers (say, analytics keys or
user IDs in external systems).

#### `phone`

Update the column to a random phone number. The country or sub-country entity of the phone number is
currently undefined behavior. There is no way to influence the randomness (and thus result), short
of tinkering with NodeJS's entropy system. For a non-random alternative, see `fixedString`.

## License

The code powering scrubbitize is released under the ISC License, as follows:

> Copyright 2021 Logixboard Inc.
>
> Permission to use, copy, modify, and/or distribute this software for any
> purpose with or without fee is hereby granted, provided that the above
> copyright notice and this permission notice appear in all copies.
>
> THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
> REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
> AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
> INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
> LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
> OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
> PERFORMANCE OF THIS SOFTWARE.

### Exceptions

- This README and all other documentation are released under the [Creative
  Commons CC0 1.0
  Universal](https://creativecommons.org/publicdomain/zero/1.0/)

- `src/pg-smart-tags.ts` and `test/utils/pg-smart-tags.test.ts` were adapted
  from
  [Graphile](https://github.com/graphile/graphile-engine/blob/9d6c29e3505844ca64020fb6850093a7678a0fa4/packages/graphile-build-pg/__tests__/tags.test.js)
  and as such retain their original MIT License.
